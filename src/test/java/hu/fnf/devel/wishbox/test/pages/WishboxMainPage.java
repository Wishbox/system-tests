package hu.fnf.devel.wishbox.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.Set;

public class WishboxMainPage {
    private static final By PLUS_SIGN = By.className("fa-plus");
    private static final String WISHBOX_PAGE_TITLE = "WishBox Admin v1.0";
    private static final By GOOGLE_PLUS_SIGNIN_BUTTON = By.className("abcRioButtonContentWrapper");
    private WebDriver driver;

    public WishboxMainPage(WebDriver driver, String url) {
        this.driver = driver;
        this.driver.get(url);
    }

    public void seeAddWishButton() {
        WebDriverWait wait = new WebDriverWait(driver, 30); //seconds
        driver.findElement(GOOGLE_PLUS_SIGNIN_BUTTON).click();

        wait.until(ExpectedConditions.titleIs(WISHBOX_PAGE_TITLE));
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(PLUS_SIGN));
        driver.getTitle();

    }

    public void login(String test_user_name, String test_user_passwd) {

        driver.findElement(GOOGLE_PLUS_SIGNIN_BUTTON).click();

        Set<String> handles = driver.getWindowHandles();
        String parent = handles.iterator().next(); // parent

        driver.switchTo().window((String) handles.toArray()[1]); //child

        driver.findElement(GoogleLoginPage.EMAIL_FIELD).sendKeys(System.getenv("TEST_USER_NAME"));
        driver.findElement(GoogleLoginPage.NEXT_BUTTON).click();

        WebDriverWait wait = new WebDriverWait(driver, 30); //seconds
        wait.until(ExpectedConditions.visibilityOfElementLocated(GoogleLoginPage.PASSWD_FIELD));

        driver.findElement(GoogleLoginPage.PASSWD_FIELD).sendKeys(System.getenv("TEST_USER_PASSWD"));
        driver.findElement(GoogleLoginPage.SIGN_IN_BUTTON).click();

        handles = driver.getWindowHandles();
        Iterator<String> it = handles.iterator();
        while (it.hasNext()) {
            String id = it.next();
            if (!id.equalsIgnoreCase(parent)) {
                driver.switchTo().window(id);
                driver.close();
            }
        }

        driver.switchTo().window(parent);
        driver.findElement(GOOGLE_PLUS_SIGNIN_BUTTON).click();
    }
}
