package hu.fnf.devel.wishbox.test;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

public class AddingNewWish {
    @Given("^a \"([^\"]*)\" field$")
    public void aField(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I press the plus sign$")
    public void iPressTheSign() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I can see the new wish at: (.*)")
    public void iCanSeeTheNewWishAt(List<String> ids) throws Throwable {
        for (String id : ids) {
            System.out.println(id);
        }
        throw new PendingException();
    }

    @When("^I enter a new wishcard like \"([^\"]*)\"$")
    public void iEnterANewWishcardLike(String wishtext) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
