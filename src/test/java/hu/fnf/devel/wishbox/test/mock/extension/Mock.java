package hu.fnf.devel.wishbox.test.mock.extension;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.common.SingleRootFileSource;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.standalone.JsonFileMappingsLoader;

import java.io.File;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class Mock {

    public Mock(String recordMappingsRoot, String pathMatching, String proxiedFrom,
                WireMockConfiguration wireMockConfiguration) {
        File files = getFileDir(recordMappingsRoot);
        File mappings = getMappings(recordMappingsRoot);

        WireMockServer wireMockServer = new WireMockServer(wireMockConfiguration
                .fileSource(new SingleRootFileSource(new File(recordMappingsRoot)))
        );
        wireMockServer.stubFor(
                any(urlPathMatching(pathMatching))
                        .willReturn(aResponse()
                                .proxiedFrom(proxiedFrom)
                        )
        );
        wireMockServer.enableRecordMappings(new SingleRootFileSource(mappings), new SingleRootFileSource(files));
        wireMockServer.start();
    }

    public Mock(String playbackMappingsRoot, WireMockConfiguration wireMockConfiguration) {
        File files = getFileDir(playbackMappingsRoot);
        File mappings = getMappings(playbackMappingsRoot);

        WireMockServer wireMockServer = new WireMockServer(wireMockConfiguration
                .fileSource(new SingleRootFileSource(new File(playbackMappingsRoot)))
        );
        wireMockServer.loadMappingsUsing(new JsonFileMappingsLoader(new SingleRootFileSource(mappings)));
        wireMockServer.start();
    }

    private File getMappings(String mappingsRoot) {
        File mappings = new File(mappingsRoot + "/mappings/");
        if (!mappings.exists()) {
            //noinspection ResultOfMethodCallIgnored
            mappings.mkdirs();
        }
        return mappings;
    }

    private File getFileDir(String mappingsRoot) {
        File files = new File(mappingsRoot + "/__files/");
        if (!files.exists()) {
            //noinspection ResultOfMethodCallIgnored
            files.mkdirs();
        }
        return files;
    }
}

