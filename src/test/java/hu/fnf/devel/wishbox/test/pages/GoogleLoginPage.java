package hu.fnf.devel.wishbox.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleLoginPage {
    static final By EMAIL_FIELD = By.id("Email");
    static final By NEXT_BUTTON = By.id("next");
    static final By PASSWD_FIELD = By.id("Passwd");
    static final By SIGN_IN_BUTTON = By.id("signIn");
    static final String MY_ACCOUNT_PAGE_TITLE = "My Account";
    private WebDriver driver;

    public GoogleLoginPage(WebDriver driver, String url) {
        this.driver = driver;
        this.driver.get(url);
    }

    public void login(String userName, String userPasswd) {
        driver.findElement(EMAIL_FIELD).sendKeys(System.getenv("TEST_USER_NAME"));
        driver.findElement(NEXT_BUTTON).click();

        WebDriverWait wait = new WebDriverWait(driver, 30); //seconds
        wait.until(ExpectedConditions.visibilityOfElementLocated(PASSWD_FIELD));

        driver.findElement(PASSWD_FIELD).sendKeys(System.getenv("TEST_USER_PASSWD"));
        driver.findElement(SIGN_IN_BUTTON).click();

        wait.until(ExpectedConditions.titleIs(MY_ACCOUNT_PAGE_TITLE));
    }
}
