package hu.fnf.devel.wishbox.test;

import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import hu.fnf.devel.wishbox.test.mock.extension.Mock;
import hu.fnf.devel.wishbox.test.pages.GoogleLoginPage;
import hu.fnf.devel.wishbox.test.pages.WishboxMainPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public class AccessPageWithGooleAccount {

    private static final int STUBPORT = 8081;
    private static final int GATEWAYPORT = 8080;
    private static final String GATEWAY_URL = "http://localhost:" + GATEWAYPORT;
    private static final String STUBGW_URL = "http://localhost:" + STUBPORT;
    private WebDriver driver = new FirefoxDriver();
    private MongodExecutable mongodExecutable;
    private Process process;

    @Given("^a running application mock$")
    public void aRunningApplicationMock() throws Throwable {
        MongodStarter starter = MongodStarter.getDefaultInstance();

        int port = 27017;
        IMongodConfig mongodConfig = null;
        try {
            mongodConfig = new MongodConfigBuilder()
                    .version(Version.Main.PRODUCTION)
                    .net(new Net(port, Network.localhostIsIPv6()))
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mongodExecutable = null;
        mongodExecutable = starter.prepare(mongodConfig);
        mongodExecutable.start();

        ProcessBuilder processBuilder = new ProcessBuilder(
                "java", "-jar",
                "/tmp/gateway-1.1.0.jar",
                "--server.port=" + GATEWAYPORT
        );
        processBuilder.environment().put("MONGODB_URI", "mongodb://localhost:" + port + "/test");
        processBuilder.environment().putAll(System.getenv());

        processBuilder.redirectError(new File("/tmp/err.out"));
        processBuilder.redirectOutput(new File("/tmp/std.out"));

        process = processBuilder.start();
        Thread.sleep(15000);
        List<String> headers = Arrays.asList("Content-type"
                , "Content-type"
                , "Accept"
                , "Connection"
                , "keep-alive"
                , "User-Agent"
                , "Host"
                , "Authorization"
                , "Cookie");

        WireMockConfiguration wireMockConfiguration = wireMockConfig().port(STUBPORT).recordRequestHeadersForMatching(headers);
        Mock mock = new Mock("src/test/resources/gatewayMockWithHeaders", "/*", GATEWAY_URL, wireMockConfiguration);
//
//        WireMockConfiguration wireMockConfiguration = wireMockConfig().port(STUBPORT);
//        Mock mock = new Mock("src/test/resources/gatewayMock", wireMockConfiguration);
    }

    @When("^I login with google account$")
    public void iLoginWithGoogleAccount() throws Throwable {
//        GoogleLoginPage googleLoginPage = new GoogleLoginPage(driver, "https://accounts.google.com");
//        googleLoginPage.login(System.getenv("TEST_USER_NAME"), System.getenv("TEST_USER_PASSWD"));
        WishboxMainPage wishboxMainPage = new WishboxMainPage(driver, STUBGW_URL);
        wishboxMainPage.login(System.getenv("TEST_USER_NAME"), System.getenv("TEST_USER_PASSWD"));
    }

    @Then("^I can see the wishbox main page$")
    public void iCanSeeTheWishbox() throws Throwable {
        WishboxMainPage wishboxMainPage = new WishboxMainPage(driver, STUBGW_URL);
        wishboxMainPage.seeAddWishButton();
    }

    @After
    public void after() {
        process.destroy();
        mongodExecutable.stop();
        driver.close();
    }
}
