Feature: Manage Wishcards

  Background: The user must be able to CRUD the wishes without affecting other wishcards. This feature should also
  include usability criteria and sense checks.

  Scenario: As a user I want to access the page with my g+ account
    Given a running application mock
    When I login with google account
    Then I can see the wishbox main page

#  Scenario Outline: As an authenticated user I want to add a new wish
#    Given a "Make a wish" field
#    When I enter a new wishcard like "<wishtext>"
#    And I press the plus sign
#    Then I can see the new wish at: wishes, events, notifications
#
#    Examples:
#      | wishtext        |
#      | Attack on Titan |